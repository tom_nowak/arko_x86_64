CC = g++
CFLAGS = -Wall -Wextra -pedantic -O3 -std=c++11
NASM = nasm -f elf64
INCLUDE_LIB = `pkg-config opencv --cflags`
LINK_LIB = -lX11 `pkg-config opencv --libs`

all: main.o draw.o
	$(CC) $(CFLAGS) main.o draw.o -o Bezier $(LINK_LIB)

main.o: main.cpp Makefile
	$(CC) $(CFLAGS) -c main.cpp -o main.o $(INCLUDE_LIB)

draw.o:	draw.nasm Makefile
	$(NASM) draw.nasm

clean:
	rm -f *.o
