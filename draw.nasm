section .text
global drawBezierCurve

;Function drawBezierCurve is divided into subprocedures. 
;At the beginning of each there is documenatation:
;What it is
;Arguments (registers valueof of which procedure relies on)
;Additional registers used
;Contents of registers which will be used later


;macro used only in DRAW_LINE_BRESENHAM (defined below)
;sets black pixel at (x0,y0), destroys values in registers: rax, rdi, rdx
%macro	SET_PIXEL 0 ;0 arguments - all are implicitly defined for this algorithm
	mov	rax, r15 ;row size in bytes
	imul	y0 ;rax = y0*rax : offset from y coordinate
	;Address cannot be greater than 64 bits - I do not check what is in rdx
	lea	rdi, [x0+2*x0] ;offset from x coordinate
	add	rdi, r10 ;byte address
	add	rdi, rax
	xor	ax, ax
	stosb
	stosw
%endmacro

%macro DRAW_LINE_BRESENHAM 0
	;________________________________________________
	;Draws line according to Bresenham algorithm
	;------------------------------------------------
	;Arguments:
	;rbx - x0
	;rsi - y0
	;r8 - x1
	;r9 - y1
	;r10 - pointer offset (const)
	;r15 - row size in bytes (const)
	;------------------------------------------------
	;Registers used:
	;rax - temporary and for multiplication
	;rdx - temporary, destroyed by mul
	;rdi - temporary and for stosb
	;rcx - loop counter
	;'varaibles' defined below:
	%define	x0 rbx
	%define	y0 rsi
	%define	deltax r8 ;x1 -> deltax
	%define	deltay r9 ;y1 -> deltay
	%define	pointer r10
	%define	stepx r12
	%define	stepy r13
	%define	error r14
	;________________________________________________	

;drawLineBresenham_setUpVariables:	
	;Set up variables deltax, stepx, deltay, stepy:
	;delta(x/y) = (x/y)1 - (x/y)0
	;if(delta(x/y) > 0) step(x/y) = 1; else step(x/y) = -1;
	;delta(x/y) = step(x/y)*delta(x/y);	
	;In assembly below there are many cmov instructions instead of jumps.
	;Temporary constants (cmov does not accept immediate arguments):
	mov	rax, -1
	mov	rdi, 1
	;x:
	sub	deltax, x0 ;deltax = x1-x0
	mov	error, deltax ;temporary, will be variable 'error' later
	neg	error ;-deltax
	cmovns	deltax, error ;if(-deltax>=0) deltax = -deltax
	cmovs	stepx, rdi ;if(-deltax<0) stepx=1
	cmovns	stepx, rax ;if(-deltax>=0) stepx=-1
	;y:
	sub	deltay, y0 ;deltay = y1-y0
	mov	error, deltay ;temporary, will be variable 'error' later
	neg	error ;-deltay
	cmovns	deltay, error ;if(-deltay>=0) deltay = -deltay
	cmovs	stepy, rdi ;if(-deltay<0) stepy=1
	cmovns	stepy, rax ;if(-deltay>=0) stepy=-1
	;rax, rdi and 'error' will be completely overwritten, x0, y0 will be changed
	cmp	deltax, deltay
	;if(deltax<deltay), y will be main coordinate, else: x
	jl	%%drawLineBresenham_iterateY
	
%%drawLineBresenham_iterateX:
	mov	error, deltax
	mov	rcx, deltax ;for LOOP
	neg	error ;error=-deltax
	add	deltax, deltax ;multiply by 2
	;(points are created by floating point algorithm and points from different iterations might have the same integer part)
	jz	%%endBresenham ;deltax=0 -> nothing to draw (this check is crucial!)
	add	deltay, deltay ;multiply by 2
%%drawLineBresenham_iterateX_loop:
	SET_PIXEL ;macro defined below drawLineBresenham documentation	
	;7 instructions below are meant to do (without branching)
	;error+=deltay; if(error>0) {y0+=stepy; error-=deltax;}
	add	error, deltay ;(original) error+=deltay
	mov	rax, y0 ;copy value to perform calculations regardless of if(error>0)
	mov	rdi, error ;as above
	add	rax, stepy ;(copy of) y0+=step
	sub	rdi, deltax ;(copy of) error-=deltax
	cmp	error, 0
	cmovg	y0, rax ;if(error>0) y0 = y0+stepy
	cmovg	error, rdi ;if(error>0) error = error-deltax
	add	x0, stepx ;standard iteration
	loop	%%drawLineBresenham_iterateX_loop
	jmp	%%endBresenham
		
%%drawLineBresenham_iterateY:
	mov	error, deltay
	mov	rcx, deltay ;for LOOP
	neg	error ;error=-deltay
	add	deltax, deltax ;multiply by 2
	add	deltay, deltay ;multiply by 2
	;jz	%%endBresenham - not needed, because if deltax<deltay then deltay>0 (always deltax>=0)
%%drawLineBresenham_iterateY_loop:
	SET_PIXEL ;macro defined below drawLineBresenham documentation	
	;7 instructions below are meant to do (without branching)
	;error+=deltax; if(error>0) {x0+=stepx; error-=deltay;}
	add	error, deltax ;(original) error+=deltax
	mov	rax, x0 ;copy value to perform calculations regardless of if(error>0)
	mov	rdi, error ;as above
	add	rax, stepx ;(copy of) x0+=step
	sub	rdi, deltay ;(copy of) error-=deltay
	cmp	error, 0
	cmovg	x0, rax ;if(error>0) x0 = x0+stepx
	cmovg	error, rdi ;if(error>0) error = error-deltay
	add	y0, stepy ;standard iteration
	loop	%%drawLineBresenham_iterateY_loop
	
%%endBresenham:

	%undef	x0
	%undef	y0
	%undef	deltax
	%undef	deltay
	%undef	pointer
	%undef	stepx
	%undef	stepy
	%undef	error
%endmacro ;DRAW LINE BRESENHAM



drawBezierCurve:
	;________________________________________________
	;Function called from UI to redraw Bezier curve
	;------------------------------------------------
	;Arguments:
	;rdi - pointer to image data
	;rsi - pointer to array of points (5 points, each 64 bits: int x, int y)
	;rdx - row size in bytes
	;rcx - image size in bytes
	;------------------------------------------------
	;Result:
	;All previous image content is cleared, only Bezier curve is left.
	;________________________________________________
	push	rbx
	push	r12
	push	r13
	push	r14
	push	r15
	mov	r15, rdx ;rdx will be destroyed later by mul, row size must be constant

clearImage:
	;________________________________________________
	;Set all pixels to white
	;------------------------------------------------
	;Arguments:
	;rdi - address of image data
	;rcx - image size in bytes
	;------------------------------------------------
	;Registers used:
	;al - byte to fill image with
	;r10 - copied value of rdi
	;------------------------------------------------
	;Registers after:
	;rsi - pointer to array of points (5 points, each 64 bits: int x, int y)
	;r10 - pointer to image data (saved previous value of rdi)
	;r15 - row size in bytes
	;________________________________________________
		
	mov	r10, rdi
	mov	al, 0xff
	rep	stosb ;fill rcx bytes starting at rdi, with byte al
	;end of: clearImage

loadPoints:
	;________________________________________________
	;Read points from memory (5 points, each 64 bits: int x, int y)
	;and changes coordinates system - relative to one of points
	;------------------------------------------------
	;Arguments:
	;rsi - pointer to array of points (const)
	;r15 - row size in bytes (const)
	;------------------------------------------------
	;Registers used:
	;rax - load point 4, and for multiplication (calculating address of point4 - y offset)
	;rbx - save (point0.x-point4.x) for future use
	;rdx - not used, but 	destroyed by mul
	;rsi - save (point0.y-point4.y) for future use
	;r10 - pointer to image data (will be changed - relative to point4)
	;r11 - for calculating address of point4 (x offset)
	;xmm0 - load points 0 and 1
	;xmm1 - load points 2 and 3	
	;xmm2 - copy point4 (to subtraxt it from other points)
	;------------------------------------------------
	;Registers after:
	;rbx - point0.x RELATIVE TO POINT4
	;rsi - point0.y RELATIVE TO POINT4
	;r10 - pointer to image data with offset (address of point4)
	;r15 - row size in bytes
	;xmm0 - (point0.x, point0.y, point1.x, point1.y) RELATIVE TO POINT4
	;xmm1 - (point2.x, point2.y, point3.x, point3.y) RELATIVE TO POINT4
	;________________________________________________
	;Load Unaligned Integer 128 Bits - points in pairs
	lddqu	xmm0, [rsi] ;points 0 and 1 are now in xmm0
	lddqu	xmm1, [rsi+16] ;points 2 and 3 are now in xmm1
	mov	rax, [rsi+32] ;point 4 is in rax
	movq	xmm2, rax ;...and in lower half of xmm2 (will be used later in xmm 4,5 calculation)
	movlhps	xmm2, xmm2 ;...and in higher half of xmm2 (low->high packed single)
	
	;There will be affine transformation of curve in order to simplify calculations:
	;point4 will be subtracted from all points, so in new coordinate system it will be
	;(0,0), which simplifies calculations; addresses will be relative to point4 address.
	
	;calculate address of point4:
	mov	r11d, eax ;point4.x (mov automatically sets high dword of r11 to 0)
	shr	rax, 32 ;point4.y
	mul	r15 ;rax = rax*r15 = point4.y * (row size in bytes) - offset from y
	lea	r11, [r11+2*r11] ;point4.x * 3 - offset from x
	add	r10, r11
	add	r10, rax ;address of point4
	psubd 	xmm0, xmm2 ;point 0,1 -= point 4 (packed sub dword)
	psubd	xmm1, xmm2
	
	;Values in xmm will be converted to floating-point for computations.
	;First point of the Bezier curve is point0, I can save it now, before
	;conversions in xmm (I am using registers rbx, rsi, because I wrote
	;procedure drawLineBresenham earlier and it uses there registers).
	movq	rsi, xmm0
	movsxd	rbx, esi
	sar	rsi, 32	
	;end of: loadPoints
	
initializeAlgorithm:
	;________________________________________________
	;Sets up algorithm of rendering Bezier curve
	;------------------------------------------------
	;Arguments:
	;xmm0 - int points 1(high), 0(low) (after transformation - relative to point 4)
	;xmm1 - int points 3(high), 2(low)
	;------------------------------------------------
	;Registers used:
	;rax - to load constants into xmm
	;r11 - save number of iterations of the main loop
	;xmm2, 3 - temporary to multiply coefficients of precalculated Bernstein polynomials by points
	;mm(x)0 - to enable conversion to 4-float vector in xmm
	;xmm4 - to help loading constants into xmm registers
	;xmm 9,10 - save values which will be variables (parameters to sample Bezier curve)
	;xmm 11-15 - save values which will be constant in the whole function
	;------------------------------------------------
	;Registers after:
	;rbx - int point0.x RELATIVE TO POINT4
	;rsi - int point0.y RELATIVE TO POINT4
	;r10 - uint pointer to image data with offset (address of point4)
	;r11 - number of iterations of the main loop
	;r15 - uint row size in bytes
	;xmm9 - float (t,t,t+dt/2,t+dt/2), t-parameter from (0,1), dt - step
	;xmm10 - float (s,s,s-dt/2,s-dt/2), s=t-1
	;xmm11 - float (1*point0, 1*point0) (const) := (point0', point0')
	;xmm12 - float (4*point1, 4*point1) (const) := (point1', point1')
	;xmm13 - float (6*point2, 6*point2) (const) := (point2', point2')
	;xmm14 - float (4*point3, 4*point3) (const) := (point3', point3')
	;xmm15 - (dt, dt, dt, dt) (const)
	;________________________________________________

	;point2', point 3':
	;set up the xmm terror machine (may be inefficient, but this beast needs special treatment; I do it only once):
	;multiply points in xmm 0,1 by (precalculated) corresponding Bernstein polynomial coefficients and convert to float
	mov	rax, 0x400000004 ;(4 32bit, 4 32bit), 4 comes from Newton's binomial coefficient: (4 over 1)
	movq	xmm4, rax ;xmm4(low) := (4,4) (will be used later to load constant into high quadwords of xmm 3,2)
	mov	rax, 0x600000006 ;(6 32bit, 6 32bit), 6 comes from Newton's binomial coefficient: (4 over 2)
	movq	xmm3, rax ;xmm3 := (0,0,6,6)
	movlhps	xmm3, xmm4 ;xmm3 := (4,4,6,6)
	pmulld	xmm3, xmm1 ;xmm3 := (4*point3, 6*point2) (Multiply Packed Signed Dword Integers and Store Low Result)
	movdq2q	mm0, xmm3 ;point2'
	cvtpi2ps xmm13, mm0 ;(low) := (float) point2' (Convert Packed Dword Integers to Packed Single-Precision FP Values)
	movlhps	xmm13, xmm13 ;xmm13(high) := xmm13(low) = point2' (duplicate value)
	movhlps	xmm3, xmm3
	movdq2q	mm0, xmm3 ;point3'
	cvtpi2ps xmm14, mm0 ;(low->low float) xmm14(low) := (float) point3'
	movlhps	xmm14, xmm14 ;xmm14 = (point3', point3')
	
	;point0', point1':
	;xmm4(low) (4,4) loaded earlier
	mov	rax, 0x100000001 ;binomial coefficient 4 over 4
	movq	xmm2, rax ;xmm2 := (0,0,1,1)
	movlhps	xmm2, xmm4 ;xmm2 := (4,4,1,1)
	pmulld	xmm2, xmm0 ;xmm2 := (4*point1, 1*point0)
	movdq2q	mm0, xmm2 ;point0'
	cvtpi2ps xmm11, mm0  ;xmm11(low) := (float) xmm2(low) = (float) point0'
	movlhps	xmm11, xmm11 ; xmm11=(point0', point0') 
	movhlps	xmm2, xmm2 ;(high->low) point1'
	movdq2q	mm0, xmm2 ;point1'
	cvtpi2ps xmm12, mm0 ;xmm12(low) := (float) point1'
	movlhps xmm12, xmm12 ;xmm12 = (point1', point1')
	
	;t, s, dt:
	mov	rax, 0x3f8000003f800000 ;2x precalculated IEEE 754 float 32 bit: (1.0, 1.0)
	movq	xmm10, rax ;(0, 0, 1.0, 1.0)
	movlhps	xmm10, xmm10 ;(1.0, 1.0, 1.0, 1.0)
	mov	rax, 0x3b8000003b800000 ;2x precalculated IEEE 754 float 32 bit: (1/256, 1/256)
	movq	xmm15, rax ;(0,0,1/256,1/256)
	movq	xmm9, rax ;(0,0,1/256,1/256)
	movlhps	xmm15, xmm15 ;(1/256, 1/256, 1/256, 1/256)
	addps	xmm9, xmm15 ;(1/256, 1/256, 1/128, 1/128)
	addps	xmm15, xmm15 ;(1/128, 1/128, 1/128, 1/128)
	;do not start from 0, besause Bezier(t=0) = point0' and do not end at 1, because Bezier(t=1) = 0
	subps	xmm10, xmm9 ;(1-1/256, 1-1/256, 1-1/128, 1-1/128)
	mov	r11, 128 ;iterate from 1/128 to 1, with step 1/128 (paralelly also from 1/256 to 1-1/256)
	;end of: initializeAlgorithm
	
renderBezier:
	;________________________________________________
	;Calculates points for Bezier curve and "calls" drawLineBresenham
	;------------------------------------------------
	;Arguments (not including DRAW_LINE_BRESENHAM) arguments:
	;xmm9 - float (t,t,t+dt/2,t+dt/2), t-parameter from (0,1), dt - step
	;xmm10 - float (s,s,s-dt/2,s-dt/2), s=t-1
	;xmm11 - float (1*point0, 1*point0) (const) := (point0', point0') (const)
	;xmm12 - float (4*point1, 4*point1) (const) := (point1', point1') (const)
	;xmm13 - float (6*point2, 6*point2) (const) := (point2', point2') (const)
	;xmm14 - float (4*point3, 4*point3) (const) := (point3', point3') (const)
	;xmm15 - (dt, dt, dt, dt) (const)
	;------------------------------------------------
	;Registers used:
	;r11 - main loop counter
	;xmm0 - final pair of points ('second' in 'low', 'first' in 'high')
	;xmm1 - temporary result of multipliaction of points and arguments
	;xmm2 - copy arguments t (than t^2, than t^3)
	;mm0 - (int) point(t)
	;mm1 - (int) point(t+dt/2)
	;------------------------------------------------
	;Registers after:
	;mm0 - (int) point(t)
	;mm1 - (int) point(t+dt/2)
	;constants vectors - not changed
	;r11 - main loop counter
	;other universal registers - not changed at all
	;________________________________________________
	%define dt_const	xmm15
	%define p3_const	xmm14
	%define p2_const	xmm13
	%define p1_const	xmm12
	%define p0_const	xmm11
	%define s_flat 	 	xmm10
	%define	t_flat		xmm9
	%define	t_power		xmm2
	%define	tmp		xmm1
	%define	point		xmm0
	
	;Paralelly calculate pair of points_flat corresponding to t_flat and t+dt/2 using Horner algorithm
	;Remeber that_flat points_flat 0,1,2,3 have coordinates_flat relative to point4 (and point_flat 4 is_flat 0) and
	;already multiplied by Newton's_flat binomial coefficients: (4 over 0), (4 over 1), (4 over 2), (4 over 3) 
	;The result_flat should be: point(t) = (((p0*s_flat + p1*t)*s_flat + p2*t^2)*s_flat + p3*t^3)*s_flat and point(t+dt/2)
	
	;insert component from p0:
	movaps	t_power, t_flat ;t^1
	movaps	point, p0_const
	mulps	point, s_flat ;p0*s
	
	;add component from p1:
	movaps	tmp, p1_const
	mulps	tmp, t_flat ;p1*t
	addps	point, tmp ;po*s+p1*t
	mulps	point, s_flat ;(po*s+p1*t)*s
	
	;add component from p2:
	mulps	t_power, t_flat ;t^2
	movaps	tmp, p2_const ;p2
	mulps	tmp, t_power ;p2*t^2
	addps	point, tmp ;(po*s+p1*t)*s_flat + p2*t^2 ;***
	mulps	point, s_flat ;((po*s+p1*t)*s_flat + p2*t^2)*s
	
	;add component from p3:
	mulps	t_power, t_flat ;t^3
	movaps	tmp, p3_const
	mulps	tmp, t_power ;p3*t^3
	addps	point, tmp ;((po*s+p1*t)*s_flat + p2*t^2)*s_flat + p3*t^3
	mulps	point, s_flat ;(((po*s+p1*t)*s_flat + p2*t^2)*s_flat + p3*t^3)*s

	;end iteration and "decode" points:
	addps	t_flat, dt_const
	subps	s_flat, dt_const
	cvtps2pi mm1, point ;second - point(t+dt/2)
	movhlps	point, point ;point(low) not_flat needed anymore
	cvtps2pi mm0, point ;first_flat - point(t)
	
	%undef	dt_const
	%undef	p3_const
	%undef 	p2_const
	%undef 	p1_const
	%undef 	p0_const
	%undef 	s
	%undef	t
	%undef	tn
	%undef	tmp
	%undef	point

drawLines:
	movq	r9, mm0
	movsxd	r8, r9d ;x1
	sar	r9, 32 ;y1
	;rbx and rsi already contain proper values (x0, y0)
	DRAW_LINE_BRESENHAM ;from (x0,y0) to (x1, y1)
	movq	rsi, mm0 ;load again from mm0, because DRAW_LINE_BRESENHAM destroys its arguments
	movsx	rbx, esi ;x1
	sar	rsi, 32 ;y1
	movq	r9, mm1
	movsxd	r8, r9d ;x2
	sar	r9, 32 ;y2
	DRAW_LINE_BRESENHAM ;from (x1,y1) to (x2, y2)
	;according to my convention - save point for the next iteration:
	movq	rsi, mm1
	movsxd	rbx, esi ;old x2 - x0 in new iteration
	sar	rsi, 32 ;old y2 - y0 in new iteration
	dec	r11
	jnz	renderBezier

end:
	pop	r15
	pop	r14
	pop	r13
	pop	r12
	pop	rbx
	emms ;according to the calling convention - Empty MMX Technology State
	ret
