#ifndef DRAW_H
#define DRAW_H
#include <cstdint>
extern "C" void drawBezierCurve(void *begin, void *pointsArray, uint64_t rowBytesSize, uint64_t imageBytesSize);
#endif // DRAW_H
