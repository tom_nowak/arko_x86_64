#include <cstdio>
#include <cstdlib>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <X11/Xlib.h> // to get screen resolution, not portable
#include "draw.h"

//define TEST_PERFORMANCE
#ifdef TEST_PERFORMANCE
#include <chrono>
#include <limits>
namespace performance
{
	struct TimeData
	{
		using Duration = std::chrono::duration<uint64_t, std::nano>;
		using Clock = std::chrono::high_resolution_clock;
		
		uint64_t total; // used to calculate average time
		uint64_t min;
		uint64_t max;
		Clock::time_point lastStart; // set by calling start()
		uint64_t iterations; // used to calculate average time
	
		TimeData() // set min so that it will be overwritten by anything, other attributes are obvious
			: total(0), min(std::numeric_limits<uint64_t>::max()), max(0), iterations(0) {} 

		void start()
		{
			lastStart = Clock::now();
		}	

		void stop()
		{
			Clock::time_point now = Clock::now();
			uint64_t tmp = std::chrono::duration_cast<Duration>(now-lastStart).count();
			if(tmp < min)
				min = tmp;
			if(tmp > max)
				max = tmp;
			total += tmp;
			++iterations;
		}
	
		void end(const char *name)
		{
			if(iterations==0)
			{
				printf("\nPerformance report for %s: not called a single time\n", name);
				return;
			}
			double averageTime = total*0.000001/iterations; // change nanoseconds to milliseconds
			uint64_t min1 = min/1000000; // integer part
			uint64_t min2 = min%1000000; // fractional part
			uint64_t max1 = max/1000000;
			uint64_t max2 = max%1000000;
			printf("\nPerformance report for %s (time in milliseconds):\n"
			       "min time: %lu.%06lu\n"
			       "max time: %lu.%06lu\n"
			       "average time: %lf\n",
			       name, min1, min2, max1, max2, averageTime);
		}
	} times[5];
	
	void showResults()
	{
		times[0].end("mouseCallback");
		times[1].end("drawBezierCurve");
		times[2].end("drawPoints");
		times[3].end("cv::imshow");
		times[4].end("key press callback");
	}
} // namespace performance

#define T_P(x) x
#else // ifndef TEST_PERFORMANCE
#define T_P(x) // empty macro
#endif


void mouseCallback(int event, int x, int y, int flags, void *userData); // function called by OpenCV on mouse events

struct ImageData
{
	int chosenPointIndex; // last point picked using keyboard
	static const int RADIUS = 3; // radius of point drawn
	cv::Point2i points[5]; // Bezier curve will be based on these points
	cv::Mat image;
	uint64_t rowBytesSize; // redundant data, but save it, so it is not recomputed all the time
	uint64_t imageBytesSize; // as above 
	const char *imageName;

	ImageData(int width, int height, const char *name)
		: chosenPointIndex(0), image(height, width, CV_8UC3), imageName(name)
	{
		// some arbitrarily chosen starting values for points:
		points[0].x = width/10;
		points[0].y = height/10;
		points[1].x = width/4;
		points[1].y = height/6;
		points[2].x = width/3;
		points[2].y = 3*height/5;
		points[3].x = 7*width/10;
		points[3].y = height/8;
		points[4].x = width/2;
		points[4].y = height/4;

		rowBytesSize = (uint64_t)image.cols*(uint64_t)image.channels();
		imageBytesSize = (uint64_t)image.rows*rowBytesSize;
		cv::namedWindow(imageName, CV_WINDOW_AUTOSIZE);
		cv::setMouseCallback(imageName, mouseCallback, (void*)this);
		refreshImage();
	}
	
	~ImageData()
	{
		cv::destroyAllWindows();
	}
	
	void adjustPointToImageBounds(cv::Point2i &p)
	{
		if(p.x < RADIUS)
			p.x = RADIUS;
		if(p.y < RADIUS)
			p.y = RADIUS;
		if(p.x > image.cols - RADIUS)
			p.x = image.cols - RADIUS;
		if(p.y > image.rows - RADIUS)
			p.y = image.rows - RADIUS;
	}	
	
	void moveChosenPoint(int x, int y)
	{
		points[chosenPointIndex].x += x;
		points[chosenPointIndex].y += y;
		adjustPointToImageBounds(points[chosenPointIndex]);
	}
	
	void drawPoints()
	{
		const cv::Scalar GRAY(100, 100, 100);
		const cv::Scalar RED(0, 0, 255);
		int i = (chosenPointIndex+1)%5;
		while(i!=chosenPointIndex)
		{			
			cv::circle(image, points[i], RADIUS, GRAY, -RADIUS, CV_AA);
			i = (i+1)%5;
		}
		cv::circle(image, points[chosenPointIndex], RADIUS, RED, -RADIUS, CV_AA);
	}
	
	void refreshImage()
	{
		// Assembler function will clear previous image and draw new Bezier curve.
		T_P(performance::times[1].start();)
		drawBezierCurve((void*)image.data, (void*)points, rowBytesSize, imageBytesSize);
		T_P(performance::times[1].stop();)
		T_P(performance::times[2].start();)
		drawPoints();
		T_P(performance::times[2].stop();)
		T_P(performance::times[3].start();)
		cv::imshow(imageName, image);
		T_P(performance::times[3].stop();)
	}		
}; // class ImageData


void mouseCallback(int event, int x, int y, int flags, void *userData) // function called by OpenCV on mouse events
{
	(void)event;
	if(!(flags & cv::EVENT_FLAG_LBUTTON))
		return;
	T_P(performance::times[0].start();)
	ImageData *d = reinterpret_cast<ImageData*>(userData);
	cv::Point2i &p = d->points[d->chosenPointIndex];
	p.x = x;
	p.y = y;
	d->adjustPointToImageBounds(p);
	d->refreshImage();
	T_P(performance::times[0].stop();)
} // void mouseCallback


int main(int argc, char **argv)
{
	Display *display = XOpenDisplay(NULL);
	Screen *screen = DefaultScreenOfDisplay(display);
	const int MAX_WIDTH = screen->width;
	const int MIN_WIDTH = MAX_WIDTH<100 ? MAX_WIDTH : 100;
	const int MAX_HEIGHT = screen->height - 70; // 70 - (approximately) for toolbar
	const int MIN_HEIGHT = MAX_HEIGHT<100 ? MAX_HEIGHT : 100;
	XCloseDisplay(display);
	if(argc!=3)
	{
		printf("Usage: %s <image width (min %i, max %i)> <image height (min %i, max %i)>\n",
		       argv[0], MIN_WIDTH, MAX_WIDTH, MIN_HEIGHT, MAX_HEIGHT);
		return 1;
	}
	int width = atoi(argv[1]);
	int height = atoi(argv[2]);
	if(width<MIN_WIDTH || width>MAX_WIDTH || height<MIN_HEIGHT || height>MAX_HEIGHT)
	{
		printf("Incorect arguments. Program expects: width (min %i, max %i), height (min %i, max %i)\n",
		       MIN_WIDTH, MAX_WIDTH, MIN_HEIGHT, MAX_HEIGHT);
		return 2;
	}
	
	ImageData imageData(width, height, "Bezier curve - see instruction in terminal");
	
	printf("Press key (1,2,3,4,5) to choose one of Bezier curve control points. Active point is red.\n"
	       "You can move it with arrows on keyboard or with mouse (with left button pressed).\n"
	       "To close the program, press ESC. Do NOT close window with 'x'.\n");
	
	for(;;)
	{
		int key = cv::waitKey(0)&0xffff; // & - ignore caps lock, not casting to char, because in 256 bits for example: arrow right = Q
		T_P(performance::times[4].start();) // stop this measurement only for keypresses that change image
		switch(key)
		{
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
				imageData.chosenPointIndex = key - '1';
				imageData.refreshImage();
				T_P(performance::times[4].stop();)
				break;
			case 65361: // arrow left
				imageData.moveChosenPoint(-1,0); // if point is at the left edge of image, moveChosenPoint will do nothing
				imageData.refreshImage();
				T_P(performance::times[4].stop();)
				break;
			case 65362: // arrow up
				imageData.moveChosenPoint(0,-1);
				imageData.refreshImage();
				T_P(performance::times[4].stop();)
				break;
			case 65363: // arrow right
				imageData.moveChosenPoint(1,0);
				imageData.refreshImage();
				T_P(performance::times[4].stop();)
				break;
			case 65364: // arrow down
				imageData.moveChosenPoint(0,1);
				imageData.refreshImage();
				T_P(performance::times[4].stop();)
				break;
			case 27: // ESC
				T_P(performance::showResults();)
				return 0;
		}
	}	
}
